package com.example.victor.fractionapp;


public class Fraction {

    private int nominator;
    private int denominator;

    public Fraction () {
        nominator = 0;
        denominator = 1;
    }


    public Fraction (int n, int d) {
        nominator = n;
        denominator = d;
    }

    public Fraction (Fraction f) {
        nominator = f.nominator;
        denominator = f.denominator;
    }

    public int getNominator() {
        return nominator;
    }

    public int getDenominator() {
        return denominator;
    }

    public void IfMinus(){
        if ((denominator<0 && nominator<0)||(denominator<0 && nominator>0)){
            nominator=-nominator;
            denominator=-nominator;






        }
    }

    public int nod(int a, int b){
        while (a!=b){
            if (a>b) a=a-b;
            if (b>a) b=b-a;

        }


        return a;

    }


    public void red(Fraction frac){
        int del=nod(frac.nominator,frac.denominator);



        frac.nominator=nominator/del;
        frac.denominator=denominator/del;

    }

    public Fraction add (Fraction f2) {
        if (this.denominator == f2.denominator) {
            nominator = this.nominator + f2.nominator;
            denominator=f2.denominator;
        } else {

            nominator = nominator * f2.denominator + f2.nominator * denominator;
            denominator = denominator * f2.denominator;

        }
        red(f2);
        red(f2);


        return (f2);


        }

    public void red(){
        for (int i=2; i<this.denominator;i++)   do{
                        this.denominator=this.denominator/i;
                        this.nominator=this.nominator/i;
            }  while (this.denominator % i==0 && this.nominator % i==0 );

    }




    public Fraction sub(Fraction f2) {
        if(this.denominator==f2.denominator){
            nominator=nominator-f2.nominator;
            denominator=f2.denominator;

        }
        else{
            nominator=nominator*f2.denominator-f2.nominator*denominator;
            denominator=denominator*f2.denominator;


        } red(f2);red(f2); return(f2);
    }



    public Fraction mult(Fraction f2) {

        nominator=f2.nominator*nominator;
        denominator=f2.denominator*denominator;
        red(f2);
        red(f2);
        return(f2);
    }


    public Fraction div(Fraction f2) {

        nominator=nominator*f2.denominator;
        denominator=denominator*f2.nominator;
        red(f2);
        red(f2);
        IfMinus();
        return(f2);
    }



}

